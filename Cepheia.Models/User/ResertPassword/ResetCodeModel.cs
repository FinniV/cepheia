﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cepheia.Models.User.ResertPassword
{
    public class ResetCodeModel
    {
        public string Code { get; set; }
        public DateTime CreationDate { get; set; }
    }
}
