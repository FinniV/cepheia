﻿using Cepheia.Models.Enums.User;
using Cepheia.Models.User.Additional;
using Microsoft.AspNetCore.Identity;
using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace Cepheia.Models.User
{
    public class ApplicationUser : IdentityUser
    {
        public LoginType LoginType { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public Gender Gender { get; set; }
        public DateTime DateBirth { get; set; }
        public string Country { get; set; }
        public string City { get; set; }
        public string Postcode { get; set; }
        public string Address1 { get; set; }
        public string Address2 { get; set; }
        public string ResetPasswordCode { get; set; }
        public string ResetPasswordToken { get; set; }
        public string ConfirmEmailToken { get; set; }

        [ForeignKey("UserImage")]
        public string UserImageId { get; set; }

        public virtual UserImage UserImage { get; set; }
    }
}
