﻿using Cepheia.Models.Enums.User;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;

namespace Cepheia.Models.User.Register
{
    public class RegisterRequestAccountModel
    {
        [JsonProperty(Required = Required.Always)]
        public string Email { get; set; }

        [JsonProperty(Required = Required.Always)]
        public string FirstName { get; set; }

        [JsonProperty(Required = Required.Always)]
        public string LastName { get; set; }

        [JsonProperty(Required = Required.Always)]
        public string Password { get; set; }

        [JsonProperty(Required = Required.Always)]
        public UserRole RoleType { get; set; }

        [JsonProperty(Required = Required.Always)]
        public Gender Gender { get; set; }

        [JsonProperty(Required = Required.Always)]
        public DateTime DateBirth { get; set; }

        public string Country { get; set; }
        public string City { get; set; }
        public string Address1 { get; set; }
        public string Address2 { get; set; }
        public string Postcode { get; set; }
        public string Phone { get; set; }
        public byte[] ProfilePicture { get; set; }
    }
}
