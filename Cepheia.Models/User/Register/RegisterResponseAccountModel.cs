﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cepheia.Models.User.Register
{
    public class RegisterResponseAccountModel
    {
        public ApplicationUser User { get; set; }
        public string Token { get; set; }
        public List<string> Messages { get; set; }

        public RegisterResponseAccountModel()
        {
            Messages = new List<string>();
        }
    }
}
