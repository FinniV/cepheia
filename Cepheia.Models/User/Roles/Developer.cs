﻿using Cepheia.Models.BaseModels;
using Cepheia.Models.Interfaces.User;
using Cepheia.Models.Interfaces.User.Roles;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cepheia.Models.User.Roles
{
    public class Developer:BaseModel, IUser, IEmployee
    {
        [ForeignKey("User")]
        public string UserId { get; set; }
        public virtual ApplicationUser User { get; set; }

        public double Salary { get; set; }
    }
}
