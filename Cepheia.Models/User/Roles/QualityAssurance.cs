﻿using Cepheia.Models.BaseModels;
using Cepheia.Models.Interfaces.User.Roles;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cepheia.Models.User.Roles
{
    public class QualityAssurance : BaseModel, IUser
    {
        [ForeignKey("User")]
        public string UserId { get; set; }
        public ApplicationUser User { get; set; }
    }
}
