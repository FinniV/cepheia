﻿using Cepheia.Models.BaseModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cepheia.Models.User.Additional
{
    public class UserImage: BaseModel
    {
        public byte[] Image { get; set; }
        public string ImageGuid { get; set; }
        public string ImageName { get; set; }
        public virtual ApplicationUser ApplicationUser { get; set; }
    }
}
