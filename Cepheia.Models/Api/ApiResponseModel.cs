﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cepheia.Models.Api
{
    public class ApiResponseModel<T>: ApiResponseModel
    {
        public T Model { get; set; }
    }
    public class ApiResponseModel
    {
        public bool IsSuccess { get; set; }
        public string Message { get; set; }
    }
}
