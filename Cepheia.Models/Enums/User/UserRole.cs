﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cepheia.Models.Enums.User
{
    public enum UserRole
    {
        None = 0,
        Admin = 1,
        ProjectManager = 2,
        GroupLeed = 3,
        Developer = 4,
        QualityAssurance = 5,
        Customer = 6,
        User = 7
    }
}
