﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cepheia.Models.Enums.User
{
    public enum LoginType
    {
        None = 0,
        Email = 1
    }
}
