﻿namespace Cepheia.Models.Enums.Work
{
    public enum TaskPriority
    {
        None = 0,
        Low = 1,
        Medium = 2,
        High = 3,
        Critical = 4
    }
}
