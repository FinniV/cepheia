﻿using Cepheia.Models.BaseModels;
using System;

namespace Cepheia.Models.Authorize
{
    public class LoginResponseModel : BaseModel
    {
        public new String Id { get; set; }

        public String Token { get; set; }
        
    }
}
