﻿using Cepheia.Models.BaseModels;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cepheia.Models.Authorize
{
    public class LoginRequestModel : BaseModel
    {
        [Required(ErrorMessage = "Password can`t be empty")]
        [RegularExpression(@"^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[^\da-zA-Z]).{8,15}$", ErrorMessage = "Password must contains digits,symbols and and uppercase literas")]
        public string Password { get; set; }
        [Required(ErrorMessage ="Username can`t be empty")]
        public string UserName { get; set; }
    }
}
