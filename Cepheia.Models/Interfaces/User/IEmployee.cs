﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cepheia.Models.Interfaces.User
{
    public interface IEmployee
    {
        double Salary { get; set; }
    }
}
