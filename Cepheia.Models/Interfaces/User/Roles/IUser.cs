﻿using Cepheia.Models.User;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cepheia.Models.Interfaces.User.Roles
{
    interface IUser
    {
        [ForeignKey("User")]
        string UserId { get; set; }
        ApplicationUser User { get; set; }
    }
}
