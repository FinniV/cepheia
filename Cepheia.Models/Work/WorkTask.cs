﻿using Cepheia.Models.BaseModels;
using Cepheia.Models.Enums.Work;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cepheia.Models.Work
{
    public class WorkTask : BaseModel
    {
        public string Title { get; set; }
        public string Description { get; set; }
        public double EstimatedTime { get; set; }
        public double Spent { get; set; }
        public TaskPriority Priority { get; set; }
        public string DeveloperId { get; set; }
    }
}
