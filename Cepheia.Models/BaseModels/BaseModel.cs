﻿using System;

namespace Cepheia.Models.BaseModels
{
    public abstract class BaseModel : IDisposable
    {
        private String id;
        public String Id
        {
            get
            {
                return id ?? (id = Guid.NewGuid().ToString());
            }
            set
            {
                id = value;
            }
        }

        public DateTime? CreationDate
        {
            get;
            set;
        }

        protected BaseModel()
        {
            var now = DateTime.UtcNow;
            CreationDate = new DateTime(now.Ticks / 100000 * 100000, now.Kind);
        }

        #region IDisposable Support
        private bool disposedValue = false; // To detect redundant calls

        protected virtual void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                {
                    Id = null;
                    CreationDate = null;
                }

                disposedValue = true;
            }
        }

        ~BaseModel()
        {
            Dispose(false);
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
        #endregion
    }
}
