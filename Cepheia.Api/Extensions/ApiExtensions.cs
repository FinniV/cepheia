﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Cepheia.Api
{
    public static class ApiExtensions
    {
        public static string StringListToString(this List<string> self)
        {
            string result = string.Empty;
            for (int i = 0; i < self.Count; i++)
            {
                result += self[i] + Environment.NewLine;
            }
            return result;
        }
    }
}
