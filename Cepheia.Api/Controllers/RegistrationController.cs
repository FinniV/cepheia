﻿using Cepheia.BusinessLogic.Services;
using Cepheia.Models.Api;
using Cepheia.Models.Authorize;
using Cepheia.Models.User;
using Cepheia.Models.User.Register;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using System;
using System.Threading.Tasks;

namespace Cepheia.Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class RegistrationController : ControllerBase
    {
        private AccountService _accountService;
        private readonly SignInManager<ApplicationUser> _signInManager;
        
        public RegistrationController(AccountService accountService, SignInManager<ApplicationUser> signInManager, IConfiguration configuration)
        {
            _signInManager = signInManager;
            _accountService = accountService;
        }

        [HttpGet()]
        public string Get([FromQuery]int id, [FromQuery]string name) => $"Id is {id} and name {name}";

        [HttpPost]
        public async Task<IActionResult> Register([FromBody] RegisterRequestAccountModel model)
        {
            var genericResponseViewModel = new ApiResponseModel<RegisterResponseAccountModel>();
            try
            {
                var result = await _accountService.Register(model);

                genericResponseViewModel.Model = result;

                if (result != null && result.Messages.Count == 0)
                {
                    await _signInManager.SignInAsync(result.User, false);
                    genericResponseViewModel.IsSuccess = true;
                    return Ok(genericResponseViewModel);
                }
                genericResponseViewModel.IsSuccess = false;
                genericResponseViewModel.Message = genericResponseViewModel.Model?.Messages.StringListToString()??"Request returned with no result";
                genericResponseViewModel.Model = null;
                return BadRequest(genericResponseViewModel);
            }
            catch (Exception e)
            {
                genericResponseViewModel.IsSuccess = false;
                genericResponseViewModel.Message = e.Message;
                genericResponseViewModel.Model = null;
                return BadRequest(genericResponseViewModel);
            }
        }
    }
}
