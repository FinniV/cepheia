﻿using Cepheia.BusinessLogic.Services;
using Cepheia.Models.Api;
using Cepheia.Models.Authorize;
using Cepheia.Models.User;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using System;

namespace Cephei.Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class AccountController : ControllerBase
    {
        private readonly SignInManager<ApplicationUser> _signInManager;
        private AccountService _accountService;
        private readonly UserManager<ApplicationUser> _userManager;

        public AccountController(AccountService accountService, UserManager<ApplicationUser> userManager, SignInManager<ApplicationUser> signInManager, IConfiguration configuration)
        {
            _accountService = accountService;
            _userManager = userManager;
            _signInManager = signInManager;
        }

        [HttpGet()]
        public string Get([FromQuery]int id, [FromQuery]string name) => $"Id is {id} and name {name}";

        // POST: api/Account
        [HttpPost]
        public async System.Threading.Tasks.Task<IActionResult> PostAsync([FromBody] LoginRequestModel value)
        {
            try
            {
                var result = await _signInManager.PasswordSignInAsync(value.UserName, value.Password, false, false);

                if (result.Succeeded)
                {
                    using (var loginResponse = await _accountService.Login(value))
                    {
                        if (string.IsNullOrEmpty(loginResponse.Token))
                        {
                            return BadRequest(new ApiResponseModel
                            {
                                IsSuccess = false,
                                Message = "Can`t generate token"
                            });
                        }
                        return Ok(new ApiResponseModel<LoginResponseModel>
                        {
                            IsSuccess = true,
                            Message = "Completed",
                            Model = loginResponse
                        });
                    }
                }

                return BadRequest(result);
            }
            catch (Exception)
            {
                return BadRequest(new ApiResponseModel<string>
                {
                    IsSuccess = false,
                    Message = "Can`t find user",
                    Model = null
                });
            }

        }

    }
}
