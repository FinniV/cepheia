﻿namespace Cepheia.BusinessLogic.Common
{
    public class AuthNotify
    {
        public static readonly string InvalidEmailOrPassword = "Invalid email or password.";
        public static readonly string EmailNotVerifiedYet = "Email is not verified. To continue working with the app, please confirm your email address in a letter that was sent to the address you provided when you had registered.";
        public static readonly string IsNotExist = "User is not exist";
        public static readonly string IsNotAdmin = "You can't registered like admin";
    }
}
