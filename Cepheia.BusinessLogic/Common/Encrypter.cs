﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Security.Cryptography;
using System.Text;

namespace Cepheia.BusinessLogic.Common
{
    public class Encrypter
    {
        public static string GetSHA512(string code)
        {
            var crypt = SHA512.Create();
            var hash = new StringBuilder();
            byte[] crypto = crypt.ComputeHash(Encoding.UTF8.GetBytes(code), 0, Encoding.UTF8.GetByteCount(code));
            foreach (byte theByte in crypto)
            {
                hash.Append(theByte.ToString("x2"));
            }
            return hash.ToString();
        }
        private static readonly int AesKeySizeInBits = 128;
        private static readonly string keyStr = "zeZsSeZK+G8IsNo3gg7MLg==";
        private static readonly byte[] key;
        private static readonly string ivStr = "50MTJbFv/ek77TcUNAEorw==";
        private static readonly byte[] iv;

        static Encrypter()
        {
            key = Convert.FromBase64String(keyStr);
            iv = Convert.FromBase64String(ivStr);
        }

        public static string Encrypt(string text)
        {
            try
            {
                byte[] rawPlaintext = Encoding.UTF8.GetBytes(text);
                using (Aes aes = new AesManaged())
                {
                    aes.Padding = PaddingMode.PKCS7;
                    aes.KeySize = AesKeySizeInBits;
                    aes.Key = key;
                    aes.IV = iv;
                    using (var ms = new MemoryStream())
                    {
                        using (var cs = new CryptoStream(ms, aes.CreateEncryptor(), CryptoStreamMode.Write))
                        {
                            cs.Write(rawPlaintext, 0, rawPlaintext.Length);
                        }
                        return Convert.ToBase64String(ms.ToArray());
                    }
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
        }

        public static string Decrypt(string base64Encrypted)
        {
            byte[] cipher = Convert.FromBase64String(base64Encrypted);
            byte[] plainText = null;
            using (Aes aes = new AesManaged())
            {
                aes.Padding = PaddingMode.PKCS7;
                aes.KeySize = AesKeySizeInBits;
                aes.Key = key;
                aes.IV = iv;

                using (var ms = new MemoryStream())
                {
                    using (var cs = new CryptoStream(ms, aes.CreateDecryptor(), CryptoStreamMode.Write))
                    {
                        cs.Write(cipher, 0, cipher.Length);
                    }
                    plainText = ms.ToArray();
                }
            }
            return Encoding.UTF8.GetString(plainText);
        }
    }
}
