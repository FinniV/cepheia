﻿using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.Text;

namespace Cepheia.BusinessLogic
{
    public class AuthOptions
    {
        public const string ISSUER = "CepheiaAuthServer";
        public const string AUDIENCE = "http://217.12.198.106:8951/";
#if Debug
        public const string AUDIENCE = "http://localhost:53479/";
#endif
        const string KEY = "Xxzafm3NyRChMmZSvJgOTO8NeQ4KbRYn";
        public const int LIFETIME = 60 * 24;
        public static SymmetricSecurityKey GetSymmetricSecurityKey()
        {
            return new SymmetricSecurityKey(Encoding.ASCII.GetBytes(KEY));
        }

        public const string GoogleClientId = "211390253609-1jphg4l319kefq5nb9eggi2snagg31pt.apps.googleusercontent.com";
        public const string GoogleClientSecret = "-iewxaMNE5Pu00_OmlsqyJFD";
        public const string FacebookAppId = "1875291392768691";
        public const string FacebookAppSecret = "b882ca344375d2e6e8dd001a7ac83d6a";
        public const string GoogleMapApiKey = "AIzaSyC9Q30E9fPErxvX-_Yk500wajXeS2TmI5g";

    }
}
