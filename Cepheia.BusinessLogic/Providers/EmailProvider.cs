﻿using Cepheia.DataAccess.Config;
using Cepheia.Models.User;
using SendGrid;
using SendGrid.Helpers.Mail;
using System;
using System.Net.Mail;
using System.Threading.Tasks;

namespace Cepheia.BusinessLogic.Providers
{
    public class EmailProvider
    {
        private static string EmailServer = "cepheia.develop@gmail.com";
        private static string EmailPassword = "DevelopmentCepheia";
        private string _apiAccountUrl = "/api/account";

        public async Task<string> SendConfirmEmail(ApplicationUser user, string code)
        {
            var bodyString = $"<p>Hi {user.FirstName},</p>";
            var link = $"{CepheiaConstants.CepheiaApiUrl}{_apiAccountUrl}/confirm-email?email={user.Email}&code={code}";
            bodyString += $"<p>";
            bodyString += $"<a href=\"{link}\">Click on this link</a> to confirm your email address.</p>";
            bodyString += GetFooter();
            var result = await SendDefaultEmail(user.Email, "Please Verify Your Email Address", bodyString);
            return result;
        }

        public async Task SendTemporaryCode(ApplicationUser user, string code)
        {
            var bodyString = $"<p>Hi {user.FirstName},</p>";
            bodyString += $"<p>Here’s your temporary code: <b>{code}</b> .</p>";
            bodyString += GetFooter();
            var result = await SendDefaultEmail(user.Email, "Here’s Your Temporary Code", bodyString);
        }

        public async Task SendTemporaryPassword(ApplicationUser user, string password)
        {
            var bodyString = $"<p>Hi {user.FirstName},</p>";
            bodyString += $"<p>Here’s your temporary password: <b>{password}</b> . Please login to your account and change this password.</p>";
            bodyString += GetFooter();
            var result = await SendDefaultEmail(user.Email, "Here’s Your Temporary Password", bodyString);
        }

        private async Task<string> SendDefaultEmail(string toAddress, string subject, string body)
        {
            try
            {
                var mail = new MailMessage();
                var smtpServer = new SmtpClient("smtp.gmail.com");
                mail.From = new MailAddress(EmailServer);
                mail.To.Add(toAddress);
                mail.Subject = subject;
                mail.Body = body;
                mail.IsBodyHtml = true;
                smtpServer.Port = 587;
                smtpServer.Credentials = new System.Net.NetworkCredential(EmailServer, EmailPassword);
                smtpServer.EnableSsl = true;

                smtpServer.Send(mail);
                return "success";
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }

        private async Task<string> SendEmail(string toAddress, string subject, string body)
        {
            try
            {
                var client = new SendGridClient(CepheiaConstants.SendGridApiKey);
                var message = new SendGridMessage
                {
                    From = new EmailAddress(CepheiaConstants.EmailServer, "HiitWay"),
                    Subject = subject,
                    HtmlContent = body
                };
                message.AddTo(new EmailAddress(toAddress));
                var response = await client.SendEmailAsync(message);
                return response.StatusCode.ToString();
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }

        private string GetFooter()
        {
            var footer = $"<p>Take care, </p><p>Hiit Way Team</p>";

            return footer;
        }
    }
}
