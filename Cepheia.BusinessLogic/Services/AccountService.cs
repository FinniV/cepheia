﻿using Cepheia.BusinessLogic.Common;
using Cepheia.BusinessLogic.Providers;
using Cepheia.DataAccess.Config;
using Cepheia.DataAccess.Repositories;
using Cepheia.Models.Api;
using Cepheia.Models.Authorize;
using Cepheia.Models.Enums.User;
using Cepheia.Models.User;
using Cepheia.Models.User.Additional;
using Cepheia.Models.User.Register;
using Cepheia.Models.User.ResertPassword;
using Microsoft.AspNetCore.Identity;
using Microsoft.IdentityModel.Tokens;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace Cepheia.BusinessLogic.Services
{
    public class AccountService
    {
        private readonly SignInManager<ApplicationUser> _signInManager;
        private readonly EmailProvider _emailProvider;
        private readonly AccountRepository _accountRepository;

        public AccountService(UserManager<ApplicationUser> userManager, SignInManager<ApplicationUser> signInManager)
        {
            _signInManager = signInManager;
            _emailProvider = new EmailProvider();
            _accountRepository = new AccountRepository(userManager);
        }

        public async Task<ApiResponseModel<ApplicationUser>> GetApplicationUser(string id)
        {
            var response = new ApiResponseModel<ApplicationUser>();

            var result = new ApplicationUser();
            var user = await _accountRepository.GetUserById(id);

            if (user == null)
            {
                response.IsSuccess = false;
                response.Message = "User is not found. Please registered.";
                response.Model = null;
                return response;
            }

            result.Id = user.Id;
            result.FirstName = user.FirstName;
            result.LastName = user.LastName;
            result.Gender = user.Gender;
            result.DateBirth = user.DateBirth;
            result.Country = user.Country;
            result.City = user.City;
            result.Postcode = user.Postcode;
            result.Address1 = user.Address1;
            result.Address2 = user.Address2;

            if (user.UserImage != null)
            {
                var userImage = new UserImage
                {
                    Id = user.UserImage.Id,
                    Image = user.UserImage.Image,
                    ImageGuid = user.UserImage.ImageGuid,
                    ImageName = user.UserImage.ImageName
                };
                result.UserImage = userImage;
            }
            response.Model = result;
            response.Message = string.Empty;
            return response;
        }

        public async Task<ResetPasswordResponseAccountModel> ResetPassword(string email)
        {
            email = email?.ToLower();
            var result = new ResetPasswordResponseAccountModel();
            var user = await _accountRepository.GetUserByEmailAndLoginType(email, LoginType.Email);
            if (user == null)
            {
                result.IsSuccess = false;
                result.Message = "This email address does not exist.";
                return result;
            }

            if (!user.EmailConfirmed)
            {
                result.IsSuccess = false;
                result.Message = "You can't reset your password because your email address has not been verified yet.";
                return result;
            }

            var password = await _accountRepository.GenerateTemporaryPassword(user);

            await Task.Run(async () =>
            {
                await _emailProvider.SendTemporaryPassword(user, password);
            });

            result.IsSuccess = true;
            result.Message = "A temporary password has been sent to your email.";

            return result;
        }

        public async Task<ResetPasswordCodeResponseModel> GetResetPasswordCode(string email)
        {
            email = email?.ToLower();
            var result = new ResetPasswordCodeResponseModel();
            var user = await _accountRepository.GetUserByEmailAndLoginType(email, LoginType.Email);

            if (user == null)
            {
                result.IsSuccess = false;
                result.Message = "This email address does not exist.";
                return result;
            }

            if (!user.EmailConfirmed)
            {
                result.IsSuccess = false;
                result.Message = "You can't reset your password because your email address has not been verified yet.";

                return result;
            }

            var resetCodeView = new ResetCodeModel();
            var resetCode = new Random().Next(1000, 10000).ToString();
            var now = DateTime.UtcNow;
            resetCodeView.Code = resetCode;
            resetCodeView.CreationDate = new DateTime(now.Ticks / 100000 * 100000, now.Kind);

            user.ResetPasswordCode = Encrypter.Encrypt(JsonConvert.SerializeObject(resetCodeView));

            var codeResult = await _accountRepository.GeneratePasswordResetToken(user);

            if (!codeResult)
            {
                result.IsSuccess = false;
                result.Message = "Some error happen, please try later.";
                return result;
            }
            await Task.Run(async () =>
            {
                await _emailProvider.SendTemporaryCode(user, resetCode);
            });

            result.IsSuccess = true;
            result.Message = "A code has been sent to your email.";
            return result;
        }

        public async Task<ResetPasswordResponseAccountModel> ChangePasswordRequest(string email, string code, string password)
        {
            email = email?.ToLower();
            var result = new ResetPasswordResponseAccountModel();
            var user = await _accountRepository.GetUserByEmailAndLoginType(email, LoginType.Email);

            if (user == null)
            {
                result.IsSuccess = false;
                result.Message = "This email address does not exist.";
                return result;
            }
            if (!user.EmailConfirmed)
            {
                result.IsSuccess = false;
                result.Message = "You can't reset your password because your email address has not been verified yet.";
                return result;
            }
            if (string.IsNullOrEmpty(user.ResetPasswordCode) || string.IsNullOrEmpty(user.ResetPasswordToken))
            {
                result.IsSuccess = false;
                result.Message = "You can't reset your password because your password has changed.";

                return result;
            }

            var encriptCode = code;
            var decrypt = Encrypter.Decrypt(user.ResetPasswordCode);
            var resetView = JsonConvert.DeserializeObject<ResetCodeModel>(decrypt);

            if (resetView == null || resetView.Code != encriptCode)
            {
                result.IsSuccess = false;
                result.Message = "A code is invalid. Please try again.";
                return result;
            }

            DateTime now = DateTime.UtcNow;
            var dateNow = new DateTime(now.Ticks / 100000 * 100000, now.Kind);

            if (dateNow > resetView.CreationDate.AddMilliseconds(CepheiaConstants.ResetPasswordCodeExpireMs))
            {
                result.IsSuccess = false;
                result.Message = "A code is expired. Please try again.";
                return result;
            }

            var restoreCode = await _accountRepository.ResetPassword(user, password);

            if (!restoreCode.Succeeded)
            {
                result.IsSuccess = false;
                result.Message = restoreCode.Errors.FirstOrDefault()?.Description;
                return result;
            }
            result.IsSuccess = true;
            result.Message = string.Empty;
            return result;
        }

        public async Task<LoginResponseModel> Login(LoginRequestModel view)
        {
            var user = await _accountRepository.GetUserByEmailAndLoginType(view.UserName, LoginType.Email);
            var token = GetTokenFromIdentity(await GetIdentityByUser(user));
            var result = new LoginResponseModel
            {
                Token = token,
                Id = user.Id
            };
            return result;
        }

        public async Task<RegisterResponseAccountModel> Register(RegisterRequestAccountModel model)
        {
            var responseModel = new RegisterResponseAccountModel();
            if (model.RoleType == UserRole.Admin)
            {
                responseModel.Messages.Add(AuthNotify.IsNotAdmin);
                return responseModel;
            }

            var user = new ApplicationUser
            {
                UserName = model.Email,
                Email = model.Email,
                LoginType = LoginType.Email,
                FirstName = model.FirstName,
                LastName = model.LastName,
                Address1 = model.Address1,
                Address2 = model.Address2,
                City = model.City,
                Gender = model.Gender,
                DateBirth = model.DateBirth,
                Country = model.Country,
                PhoneNumber = model.Phone,
                Postcode = model.Postcode,
                ConfirmEmailToken = Guid.NewGuid().ToString()
            };
            if (model.ProfilePicture != null)
            {
                var profilePicture = new UserImage();
                profilePicture.ImageGuid = Guid.NewGuid().ToString();
                profilePicture.Image = model.ProfilePicture;
                profilePicture.ImageName = $"{model.FirstName}_{model.LastName}";
                user.UserImage = profilePicture;
            }

            var result = await _accountRepository.CreateUserWithPassword(user, model.Password);
            if (!result.Succeeded)
            {
                foreach (var error in result.Errors)
                {
                    responseModel.Messages.Add(error.Description);
                }
                return responseModel;
            }

            result = await _accountRepository.AddToRole(user, model.RoleType.ToString());
            if (!result.Succeeded)
            {
                foreach (var error in result.Errors)
                {
                    responseModel.Messages.Add(error.Description);
                }
                return responseModel;
            }

            var token = await GenerateJwtToken(model.Email, model.Password, LoginType.Email, true);
            if (string.IsNullOrEmpty(token))
            {
                responseModel.Messages.Add(AuthNotify.IsNotExist);
                return responseModel;
            }
            user = await _accountRepository.GetUserByEmailAndLoginType(model.Email, LoginType.Email);

            responseModel.User = user;
            responseModel.Token = token;

            await Task.Run(async () =>
            {
                await _emailProvider.SendConfirmEmail(user, user.ConfirmEmailToken);
            });

            return responseModel;
        }

        public async Task<string> ConfirmEmail(string email, string code)
        {
            if (string.IsNullOrEmpty(email) || string.IsNullOrEmpty(code))
            {
                return "Email or code is empty.";
            }

            var user = await _accountRepository.GetUserByEmailAndLoginType(email, LoginType.Email);
            if (user == null)
            {
                return $"Unable to load user with email '{email}'.";
            }

            if (user.EmailConfirmed || string.IsNullOrEmpty(user.ConfirmEmailToken))
            {
                return $"The email is confirmed!";
            }

            var result = await _accountRepository.ConfirmEmail(user, code);
            if (!result)
            {
                return $"Error confirming email for user with email '{email}'.";
            }

            return "The email confirmation was successful!";
        }

        private async Task<IdentityResult> CreateUserWithExternalProvider(ApplicationUser user)
        {
            IdentityResult result = await _accountRepository.CreateUser(user);
            if (!result.Succeeded)
            {
                return result;
            }
            result = await _accountRepository.AddToRole(user, UserRole.User.ToString());
            return result;
        }

        private async Task<string> GenerateJwtToken(string email, string password, LoginType loginType, bool withPassword)
        {
            var identity = await GetIdentity(email, password, loginType, withPassword);
            if (identity == null)
            {
                return string.Empty;
            }
            return GetTokenFromIdentity(identity);
        }

        private static string GetTokenFromIdentity(ClaimsIdentity identity)
        {
            var now = DateTime.UtcNow;
            var jwt = new JwtSecurityToken(
                issuer: AuthOptions.ISSUER,
                audience: AuthOptions.AUDIENCE,
                notBefore: now,
                claims: identity.Claims,
                expires: now.Add(TimeSpan.FromMinutes(AuthOptions.LIFETIME)),
                signingCredentials: new SigningCredentials(AuthOptions.GetSymmetricSecurityKey(), SecurityAlgorithms.HmacSha256));

            var encodedJwt = new JwtSecurityTokenHandler().WriteToken(jwt);
            return encodedJwt;
        }

        private async Task<ClaimsIdentity> GetIdentity(string email, string password, LoginType loginType, bool withPassword, ApplicationUser user=null)
        {
            email = email?.ToLower();
            if (withPassword)
            {
                var signIn = await _signInManager.PasswordSignInAsync(email, password, true, lockoutOnFailure: true);
                if (!signIn.Succeeded)
                {
                    return null;
                }
            }
            if (user==null)
            {
                user = await _accountRepository.GetUserByEmailAndLoginType(email, loginType);
            }
            return await GetIdentityByUser(user);
        }

        private async Task<ClaimsIdentity> GetIdentityByUser(ApplicationUser user)
        {
            if (user == null)
            {
                return null;
            }
            var role = await _accountRepository.GetRoles(user);
            var claims = new List<Claim>
            {
                new Claim("Id", user.Id),
                new Claim(ClaimsIdentity.DefaultNameClaimType, user.Email),
                new Claim(ClaimsIdentity.DefaultRoleClaimType, role.FirstOrDefault())
            };
            ClaimsIdentity claimsIdentity =
                new ClaimsIdentity(claims, "Token", ClaimsIdentity.DefaultNameClaimType,
                    ClaimsIdentity.DefaultRoleClaimType);
            return claimsIdentity;
        }
    }
}
