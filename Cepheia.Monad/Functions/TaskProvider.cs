﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Cepheia.Monad.Functions
{
    public class TaskProvider
    {
        private static TaskProvider provider;
        public static TaskProvider Provider
        {
            get
            {
                if (provider==null)
                {
                    provider = new TaskProvider();
                }
                return provider;
            }
        }
        public void TaskWrapper<TAsyncResult>(Func<Task<TAsyncResult>> asyncAction, Action<TAsyncResult> continueWithAction)
        {
            var result = default(TAsyncResult);
            Task.Factory.StartNew(async () =>
            {
                result = await asyncAction();
            }).Unwrap().ContinueWith(task =>
            {
                continueWithAction(result);
            }, TaskScheduler.FromCurrentSynchronizationContext());
        }
    }
}
