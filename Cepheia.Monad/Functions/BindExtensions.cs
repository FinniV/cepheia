﻿using Cepheia.Models.BaseModels;
using Cepheia.Monad.Monads;
using System;
using System.Collections.Generic;
using System.Text;

namespace Cepheia.Monad.Functions
{
    public static class BindExtensions
    {
        public static TSelf Bind<TSelf>(this TSelf self) where TSelf: BaseModel
        {
            if (self!=null)
            {
                return self;
            }
            return null;
        }

        public static Maybe<TSelf> Bind<TSelf>(this Maybe<TSelf> self) where TSelf: class
        {
            if (self.IsHasValue)
            {
                return self;
            }
            return null;
        }
    }
}
