﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Cepheia.Monad.Singleton
{
    public static class BaseSingleton<TSingleton> where TSingleton:class,new()
    {
        private static readonly object _locker = new object();

        private static TSingleton _instance;
        public static TSingleton Instance
        {
            get
            {
                lock (_locker)
                {

                    if (_instance == null)
                    {
                        _instance = new TSingleton();
                    }
                }
                return _instance;
            }
        }
    }
}
