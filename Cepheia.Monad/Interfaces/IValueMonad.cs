﻿namespace Cepheia.Monad.Interfaces
{
    public interface IValueMonad<TValue> : IMonad
    {
        TValue Value { get; set; }
    }
}
