﻿using Cepheia.Monad.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;

namespace Cepheia.Monad.Monads
{
    public class Maybe<TValue> : IValueMonad<TValue> where TValue : class
    {
        public TValue Value { get; set; }
        public bool IsHasValue {
            get
            {
                return Value != null;
            }
        }
    }
}
