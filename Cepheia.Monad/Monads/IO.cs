﻿using Cepheia.Monad.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;

namespace Cepheia.Monad.Monads
{
    public class IO<TResult> : IValueMonad<TResult>
    {
        public TResult Value { get; set; }
    }
}
