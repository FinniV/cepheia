﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cepheia.Core.Configuration
{
    public static class Config
    {
        public static string ApiUrl { get; } = "https://localhost:44336/api";
        public static string IdentityUrl { get; } = ApiUrl + "/account";
    }
}
