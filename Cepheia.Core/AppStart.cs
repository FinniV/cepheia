﻿using Cepheia.Core.ViewModels.StartUp;
using MvvmCross.Navigation;
using MvvmCross.ViewModels;

namespace Cepheia.Core
{
    public class AppStart : MvxAppStart<StartupViewModel>
    {
        public AppStart(IMvxApplication application, IMvxNavigationService navigationService) : base(application, navigationService)
        {
        }
    }
}
