﻿using Cepheia.Core.Interfaces.Services;
using Cepheia.Core.ViewModels.Base;
using MvvmCross.Navigation;

namespace Cepheia.Core.ViewModels
{

    public class SecondViewModel : BaseViewModel
    {
        #region Fields

        #endregion

        #region Properties

        #endregion

        #region lifecycle

        public SecondViewModel(IMvxNavigationService navigationService, IAlertService alertService) : base(navigationService,alertService)
        {

        }
        #endregion

        #region Commands

        #endregion

        #region Methods

        #endregion
    }

}
