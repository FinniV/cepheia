﻿using System.Collections.Generic;
using Cepheia.Core.ViewModels.Base;
using Cepheia.Models.Authorize;
using MvvmCross.Commands;
using System.ComponentModel.DataAnnotations;
using Cepheia.Core.Interfaces.Services;
using Cepheia.Core.Interfaces.Services.Authorization;
using Cepheia.Core.Interfaces.Services.Secure;
using System.Threading.Tasks;
using Cepheia.Core.ViewModels.Registration;
using MvvmCross.Navigation;
using Plugin.SecureStorage;
using Cepheia.Core.Configuration;
using Cepheia.Models.Api;

namespace Cepheia.Core.ViewModels.StartUp
{

    public class StartupViewModel : BaseViewModel
    {
        #region Fields
        private IAuthorizationService _authorizationService;
        #endregion

        #region Properties



        private LoginRequestModel _loginModel;
        public LoginRequestModel LoginModel
        {
            get => _loginModel;
            set => SetProperty(ref _loginModel, value);
        }





        #endregion

        #region lifecycle

        public StartupViewModel(IMvxNavigationService navigationService, IAlertService alertService, IAuthorizationService authorizationService) : base(navigationService,alertService)
        {
            _authorizationService = authorizationService;
            LoginModel = new LoginRequestModel
            {
                Password = "Test1234!!",
                UserName = "developer1@gmail.com"
            };
        }
        #endregion

        #region Commands

        public IMvxCommand AuthorizeCommand
        {
            get
            {
                return new MvxCommand(() =>
                {
                    if (CheckCredentials(LoginModel.Password, LoginModel.UserName))
                    {

                        TaskProvider.TaskWrapper(()=> _authorizationService.AuthorizeAsync(LoginModel), CheckAuthorizationResult);
                    }
                });
            }
        }

        private void CheckAuthorizationResult(ApiResponseModel<LoginResponseModel> result)
        {
            if (result.IsSuccess)
            {
                AlertService.ShowSuccesToast("Authorized");
                CrossSecureStorage.Current.SetValue(CrossStorageConstants.TokenKey, result.Model.Token);
                var token = CrossSecureStorage.Current.GetValue(CrossStorageConstants.TokenKey);
                NavigationService.Navigate<SecondViewModel>();
                return;
            }
            AlertService.ShowErrorToast(result.Message);
        }

        public IMvxCommand NavigateToRegistrationCommand
        {
            get
            {
                return new MvxCommand(() =>
                {
                    NavigationService.Navigate<RegistrationViewModel>();
                });
            }
        }


        #endregion

        #region Methods
        private bool CheckCredentials(string password,string login)
        {
            var validationContext = new ValidationContext(LoginModel);
            var result = new List<ValidationResult>();
            if (!Validator.TryValidateObject(LoginModel, validationContext,result,true))
            {
                foreach (var validationResult in result)
                {
                    AlertService.ShowErrorToast(validationResult.ErrorMessage);
                }

                return false;
            }

            return true;
        }
        #endregion
    }

}
