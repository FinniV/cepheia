﻿using Cepheia.Core.Interfaces.Services;
using Cepheia.Core.Interfaces.Services.File;
using Cepheia.Core.Interfaces.Services.Registration;
using Cepheia.Core.ViewModels.Base;
using Cepheia.Models.Api;
using Cepheia.Models.User.Register;
using MvvmCross.Commands;
using MvvmCross.Navigation;

namespace Cepheia.Core.ViewModels.Registration
{

    public class RegistrationViewModel : BaseViewModel
    {
        #region Fields
        private IFileOpenService _fileOpenService;
        private readonly IRegistrationService _RegistrationService;
        #endregion

        #region Properties

        private RegisterRequestAccountModel _userModel;
        public RegisterRequestAccountModel UserModel
        {
            get => _userModel;
            set => SetProperty(ref _userModel, value);
        }


        #endregion

        #region lifecycle

        public RegistrationViewModel(IMvxNavigationService navigationService,IAlertService alertService,IFileOpenService openService, IRegistrationService registration) : base(navigationService,alertService)
        {
            _fileOpenService = openService;
            _RegistrationService = registration;
            UserModel = new RegisterRequestAccountModel();
        }
        #endregion

        #region Commands


        public IMvxCommand SetImageCommand
        {
            get
            {
                return new MvxAsyncCommand(async() =>
                {
                    var iOResult = await _fileOpenService.OpenImageAsync();
                    if (iOResult.Value!=null&&iOResult.Value.Length!=0)
                    {
                        UserModel.ProfilePicture = iOResult.Value;
                    }
                });
            }
        }


        public IMvxCommand RegisterProfileCommand
        {
            get
            {
                return new MvxCommand(() =>
                {
                    IsBusy = true;
                    TaskProvider.TaskWrapper(()=>_RegistrationService.RegisterAccountAsync(new RegisterRequestAccountModel
                    {
                        Email = "qwerty@gml.cm",
                        City = "Test",
                        Country = "TestCountry",
                        Password = "qwerty",
                        FirstName = "TestName",
                        LastName = "TestLastName",
                        RoleType=Models.Enums.User.UserRole.User
                    }),(ApiResponseModel<RegisterResponseAccountModel> response)=> 
                    {
                        IsBusy = false;
                    });
                });
            }
        }


        #endregion

        #region Methods



        #endregion
    }

}
