﻿using System.Threading.Tasks;
using Cepheia.Core.Interfaces.ViewModels.Base;
using Cepheia.Core.Interfaces.Services;
using MvvmCross.ViewModels;
using MvvmCross.Navigation;
using MvvmCross.Commands;
using Cepheia.Monad.Functions;

namespace Cepheia.Core.ViewModels.Base
{
    public abstract class BaseViewModel : MvxViewModel, IBaseViewModel
    {
        protected IMvxNavigationService NavigationService;
        protected IAlertService AlertService;

        private bool _isBusy = false;
        public bool IsBusy
        {
            get
            {
                return _isBusy;
            }
            set
            {
                SetProperty(ref _isBusy, value);
            }
        }
        
        public TaskProvider TaskProvider
        {
            get => TaskProvider.Provider;
        }

        public BaseViewModel(IMvxNavigationService navigationService, IAlertService alertService)
        {
            NavigationService = navigationService;
            AlertService = alertService;
        }
        
        public virtual MvxCommand GoBackCommand => new MvxCommand(() =>
        {
            NavigationService.Close(this);
        });
        
    }
    public abstract class BaseViewModel<TParameter, TResult> : MvxViewModel<TParameter, TResult>, IBaseViewModel
       where TParameter : class
       where TResult : class
    {
        protected IMvxNavigationService NavigationService;
        
        private bool _isBusy;
        public bool IsBusy { get { return _isBusy; } set { SetProperty(ref _isBusy, value); } }

        public BaseViewModel(IMvxNavigationService navigationService)
        {
            NavigationService = navigationService;

        }

        public override Task Initialize()
        {
            return base.Initialize();
        }

        public virtual MvxCommand GoBackCommand => new MvxCommand(() =>
        {
            NavigationService.Close(this);
        });
        
    }


    public abstract class BaseViewModel<TParameter> : MvxViewModel<TParameter>, IBaseViewModel
      where TParameter : class

    {
        protected IMvxNavigationService NavigationService;
        protected TParameter Parameter;
        
        private bool _isBusy;
        public bool IsBusy
        {
            get => _isBusy;
            set => SetProperty(ref _isBusy, value);
        }



        public BaseViewModel(IMvxNavigationService navigationService)
        {
            NavigationService = navigationService;

        }

        public override void Prepare(TParameter parameter)
        {
            Parameter = parameter;

        }

        public override Task Initialize()
        {
            return base.Initialize();

        }

        public virtual MvxCommand GoBackCommand => new MvxCommand(() =>
        {
            NavigationService.Close(this);
        });
        

    }
}
