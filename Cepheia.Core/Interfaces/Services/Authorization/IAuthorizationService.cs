﻿using Cepheia.Models.Api;
using Cepheia.Models.Authorize;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cepheia.Core.Interfaces.Services.Authorization
{
    public interface IAuthorizationService
    {
         Task<ApiResponseModel<LoginResponseModel>> AuthorizeAsync(LoginRequestModel loginmodel);
    }
}
