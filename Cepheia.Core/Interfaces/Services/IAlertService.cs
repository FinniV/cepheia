﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cepheia.Core.Interfaces.Services
{
    public interface IAlertService
    {
        void ShowSuccesToast(string message);
        void ShowInformationToast(string message);
        void ShowWarningToast(string message);
        void ShowErrorToast(string message);

    }
}
