﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cepheia.Core.Interfaces.Services.Secure
{
    public interface ITokenStorageService
    {
        bool WriteToken(string token);
        string GetToken();
    }
}
