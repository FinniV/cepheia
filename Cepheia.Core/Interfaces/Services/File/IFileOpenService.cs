﻿using Cepheia.Monad.Monads;
using System.Threading.Tasks;

namespace Cepheia.Core.Interfaces.Services.File
{
    public interface IFileOpenService
    {
        Task<IO<byte[]>> OpenImageAsync();
        Task<IO<byte[]>> OpenFile();
    }
}
