﻿using Cepheia.Models.Api;
using Cepheia.Models.User.Register;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Cepheia.Core.Interfaces.Services.Registration
{
    public interface IRegistrationService
    {
        Task<ApiResponseModel<RegisterResponseAccountModel>> RegisterAccountAsync(RegisterRequestAccountModel model);
    }
}
