﻿using Cepheia.Core.Configuration;
using Cepheia.Core.Interfaces.Services.Registration;
using Cepheia.Models.Api;
using Cepheia.Models.User.Register;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;
using System.Threading.Tasks;

namespace Cepheia.Core.Services.Api
{
    public class RegistrationService : BaseApiService<RegisterResponseAccountModel>, IRegistrationService
    {
        public async Task<ApiResponseModel<RegisterResponseAccountModel>> RegisterAccountAsync(RegisterRequestAccountModel model)
        {
            try
            {
                var Url = Config.ApiUrl+ "/registration";
                var request = await ExecutePostAsync(Url, model);
                RawResponse = request.Content.ReadAsStringAsync().Result;

                ApiResponse = JsonConvert.DeserializeObject<ApiResponseModel<RegisterResponseAccountModel>>(RawResponse);

                return ApiResponse;
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex);
            }
            return ApiResponse;
        }
    }
}
