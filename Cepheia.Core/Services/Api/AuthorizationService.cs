﻿using Cepheia.Core.Configuration;
using Cepheia.Core.Interfaces.Services.Authorization;
using Cepheia.Models.Api;
using Cepheia.Models.Authorize;
using Newtonsoft.Json;
using System;
using System.Diagnostics;
using System.Threading.Tasks;

namespace Cepheia.Core.Services.Api
{
    public class AuthorizationService : BaseApiService<LoginResponseModel>, IAuthorizationService
    {
        public async Task<ApiResponseModel<LoginResponseModel>> AuthorizeAsync(LoginRequestModel loginmodel)
        {
            try
            {
                var Url = Config.IdentityUrl;
                var request = await ExecutePostAsync(Url,loginmodel);
                RawResponse = request.Content.ReadAsStringAsync().Result;

                ApiResponse = JsonConvert.DeserializeObject<ApiResponseModel<LoginResponseModel>>(RawResponse);
                
                return ApiResponse;
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex);
            }
            return ApiResponse;
        }
    }
}
