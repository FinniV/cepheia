﻿using Cepheia.Core.Configuration;
using Cepheia.Models.Api;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;

namespace Cepheia.Core.Services.Api
{
    public class BaseApiService<TResponseModel>
    {
        protected ApiResponseModel<TResponseModel> ApiResponse { get; set; } = new ApiResponseModel<TResponseModel>
        {
            IsSuccess = false,
            Message = "Request failed",
            Model=default(TResponseModel)
        };

        protected string RawResponse { get; set; } = string.Empty;
        protected HttpClient CreateHttpClient()
        {
            try
            {
                var handler = new HttpClientHandler();
                handler.ClientCertificateOptions = ClientCertificateOption.Manual;
                handler.ServerCertificateCustomValidationCallback =
                    (httpRequestMessage, cert, cetChain, policyErrors) =>
                    {
                        return true;
                    };
                var httpClient = new HttpClient(handler);

                httpClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                return httpClient;
            }
            catch (Exception)
            {
                return new HttpClient();
            }
        }

        protected async Task<ApiResponseModel> PostAsync(string url, object keys)
        {
            try
            {
                var response = await ExecutePostAsync(url, keys);
                if (response.StatusCode == HttpStatusCode.Unauthorized)
                {
                    return new ApiResponseModel()
                    {
                        IsSuccess = false,
                    };
                }
                if (response.StatusCode == HttpStatusCode.Forbidden)
                {
                    return new ApiResponseModel
                    {
                        IsSuccess = false,
                    };
                }
                var responseString = await response.Content.ReadAsStringAsync();
                if (response.StatusCode != HttpStatusCode.OK)
                {
                    return new ApiResponseModel
                    {
                        IsSuccess = false,
                    };
                }
            }
            catch (Exception ex)
            {
                System.Diagnostics.Debug.WriteLine($"{this.GetType().Name}: PostAsync({url}): Exception: {ex.Message}");
                return new ApiResponseModel()
                {
                    IsSuccess = false,
                };
            }
            return new ApiResponseModel()
            {
                IsSuccess = true,
            };
        }

        protected async Task<ApiResponseModel> PostAsync(string url, string data)
        {
            var client = new HttpClient();
            var queryString = new StringContent(data);
            var response = await client.PostAsync(url, queryString);


            return null;
        }

        private async Task<HttpResponseMessage> ExecutePostAsync(string url, object keys, string token = "")
        {
            var httpClient = CreateHttpClient();
            AddToken(ref url, ref token, ref httpClient);
            if (!string.IsNullOrWhiteSpace(token))
            {
                httpClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", token);
            }
            if (keys.GetType() == typeof(List<KeyValuePair<string, string>>))
            {
                var response = await httpClient.PostAsync(url, new FormUrlEncodedContent((List<KeyValuePair<string, string>>)keys));
                return response;
            }
            else
            {
                httpClient.DefaultRequestHeaders.Accept.Clear();
                httpClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                var settings = new JsonSerializerSettings
                {
                    ContractResolver = new CamelCasePropertyNamesContractResolver(),
                    NullValueHandling = NullValueHandling.Ignore
                };

                var body = JsonConvert.SerializeObject(keys, Formatting.Indented, settings);
                var response = await httpClient.PostAsync(url, new StringContent(body, Encoding.Unicode, "application/json"));
                return response;
            }
        }

        public async Task<HttpResponseMessage> ExecutePostAsync(string url, string token = "")
        {
            var httpClient = CreateHttpClient();
            AddToken(ref url, ref token, ref httpClient);

            var response = await httpClient.PostAsync(url, new StringContent("application/json"));
            return response;
        }

        private void AddToken(ref string url, ref string token, ref HttpClient httpClient)
        {
            Debug.WriteLine($"token: {token}");
            Debug.WriteLine($"Execute url \n {url}");
            if (!string.IsNullOrWhiteSpace(token))
            {
                httpClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", token);
            }
        }

        public async Task<HttpResponseMessage> ExecutePostAsync<T>(string url, T data, string token = "")
        {
            var httpClient = CreateHttpClient();
            AddToken(ref url, ref token, ref httpClient);
            var content = JsonConvert.SerializeObject(data);
            var response = await httpClient.PostAsync(url, new StringContent(content, Encoding.Unicode, "application/json"));
            return response;
        }

        public async Task<HttpResponseMessage> ExecuteGetAsync(string url, string token = "")
        {
            var httpClient = CreateHttpClient();
            AddToken(ref url, ref token, ref httpClient);
            var response = await httpClient.GetAsync(url);
            Debug.WriteLine($"Resopnse result {response.StatusCode} , {response.RequestMessage} ");
            return response;
        }

        private async Task<HttpResponseMessage> ExecutePostAsync(string url, ICollection<byte[]> bytes, string token = "")
        {
            var httpClient = CreateHttpClient();
            AddToken(ref url, ref token, ref httpClient);
            var content = new MultipartFormDataContent();
            foreach (var array in bytes)
            {
                var byteContent = new ByteArrayContent(array);
                byteContent.Headers.ContentType = MediaTypeHeaderValue.Parse("multipart/form-data");
                var name = Guid.NewGuid().ToString();
                byteContent.Headers.ContentDisposition = new ContentDispositionHeaderValue("attachment") { FileName = name + ".jpg", Name = "file" };
                content.Add(byteContent);
            }
            var response = await httpClient.PostAsync(url, content);
            return response;
        }

        private async Task<HttpResponseMessage> ExecuteDeleteAsync(string url, string token = "")
        {
            var httpClient = CreateHttpClient();

            AddToken(ref url, ref token, ref httpClient);
            if (!string.IsNullOrWhiteSpace(token))
            {
                httpClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", token);
            }
            return await httpClient.DeleteAsync(url);
        }

        protected async Task<bool> RefreshToken(string refreshToken)
        {

            if (string.IsNullOrWhiteSpace(refreshToken))
            {
                Debug.WriteLine($"BASESERVICE: {this?.GetType()?.Name}.RefreshToken({refreshToken}): BAD REFRESH TOKEN");
                return false;
            }
            Debug.WriteLine($"BASESERVICE: {this?.GetType()?.Name}.RefreshToken({refreshToken})");
            var keys = new List<KeyValuePair<string, string>>() {
                new KeyValuePair<string, string>("grant_type", "refresh_token"),
                new KeyValuePair<string, string>("token", refreshToken)
            };

            var response = await ExecutePostAsync(Config.IdentityUrl + "/auth/refresh-token", keys);
            if (response.StatusCode == HttpStatusCode.OK)
            {
                try
                {
                    var responseString = await response.Content.ReadAsStringAsync();
                    return true;
                }
                catch { }
            }
            Debug.WriteLine($"BASESERVICE: {this?.GetType()?.Name}.RefreshToken({refreshToken}) - StatusCode not OK");
            return false;
        }

        protected async Task<bool> ReLogin()

        {
            var url = Config.IdentityUrl + "/auth/";
            return false;
        }

    }
}
