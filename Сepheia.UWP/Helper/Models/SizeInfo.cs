﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.Foundation;
using Windows.UI.Xaml;

namespace Сepheia.UWP.Helper.Models
{
    public class SizeInfo
    {
        public Size PreviousSize { get; private set; }

        private Size _newSize;
        public Size NewSize
        {
            get => _newSize;
            private set
            {
                HeightChanged = PreviousSize.Height != value.Height;
                WidthChanged = PreviousSize.Width != value.Width;
                _newSize = value;
            }
        }

        public bool HeightChanged { get; private set; }
        public bool WidthChanged { get; private set; }


        public SizeInfo(Size previousSize, Size newSize)
        {
            PreviousSize = previousSize;
            NewSize = newSize;
        }

       public static implicit operator SizeInfo(SizeChangedEventArgs sizeChangedEventArgs)
        {
            return new SizeInfo(sizeChangedEventArgs.PreviousSize, sizeChangedEventArgs.NewSize);
        }
    }
}
