﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;

namespace Сepheia.UWP.Behaviors
{
    public class PasswordBehavior : BehaviorBase<PasswordBox>
    {
        public static readonly DependencyProperty PasswordProperty =
            DependencyProperty.Register("Password", typeof(string), typeof(PasswordBehavior), new PropertyMetadata(default(string)));

        private bool _skipUpdate;

        public string Password
        {
            get { return (string)GetValue(PasswordProperty); }
            set { SetValue(PasswordProperty, value); }
        }

        protected override void ElementAttached()
        {
            AssociatedElement.PasswordChanged += PasswordBox_PasswordChanged;
        }

        protected override void ElementDetached()
        {
            AssociatedElement.PasswordChanged -= PasswordBox_PasswordChanged;
        }
        
        private void PasswordBox_PasswordChanged(object sender, RoutedEventArgs e)
        {
            _skipUpdate = true;
            Password = AssociatedElement.Password;
            _skipUpdate = false;
        }
    }
}
