﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Navigation;
using Сepheia.UWP.Helper.Controll;
using Сepheia.UWP.Interfaces;

namespace Сepheia.UWP.Behaviors
{
    public abstract class BehaviorBase<T> : DependencyObject, IBehavior
        where T : FrameworkElement
    {

        public bool IsActive { get; set; } = false;

        public T AssociatedElement { get; private set; }

        public DependencyObject AssociatedObject { get { return AssociatedElement; } }
        
        public void Attach(DependencyObject associatedObject)
        {
            if (associatedObject as T == null)
                throw new NotSupportedException("The associated object must be of type " + typeof(T).Name);
            AssociatedElement = associatedObject as T;
            ElementAttached();
        }
        
        protected virtual void ElementAttached()
        {
            AssociatedElement.Loaded += ElementLoaded;
            IsActive = true;
        }

        /// <summary>
        /// You can optionally override this method to capture the loading event of the AssociatedElement.
        /// </summary>
        protected virtual void ElementLoaded(object sender, RoutedEventArgs e)
        {
            var page = ControlHelper.GetParentPage(AssociatedElement);
            if (page != null && page.NavigationCacheMode == NavigationCacheMode.Disabled)//Subcribe to the unloading event to handle state
                page.Unloaded += PageUnloaded;
        }

        private void PageUnloaded(object sender, RoutedEventArgs e)
        {
            this.Detach();
        }

        public void Detach()
        {
            ElementDetached();
            IsActive = false;
        }

        protected virtual void ElementDetached()
        {
            var page = ControlHelper.GetParentPage(AssociatedElement);
            if (page != null && page.NavigationCacheMode == NavigationCacheMode.Disabled)//Subcribe to the unloading event to handle state
                page.Unloaded -= PageUnloaded;
            AssociatedElement.Loaded -= ElementLoaded;
        }
    }
}
