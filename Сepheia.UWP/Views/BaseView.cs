﻿using MvvmCross.Platforms.Uap.Views;
using Windows.UI.Xaml.Controls;
using Сepheia.UWP.Helper.Models;

namespace Сepheia.UWP.Views
{
    public class BaseView : MvxWindowsPage
    {
        protected double GetSizeWithCheckInRealationTo<TControll>(SizeInfo sizeChangedInfo, TControll controll, double multiply, double current) where TControll : Control
        {
            if (sizeChangedInfo.HeightChanged)
            {
                return controll.ActualHeight * multiply;
            }
            return current;
        }

        protected double GetSizeInRealationToHeight<TControll>(TControll controll, double multiply) where TControll : Control => controll.ActualHeight * multiply;

        protected double GetSizeInRealationToWidth<TControll>(SizeInfo sizeChangedInfo, TControll controll, double multiply, double current) where TControll : Control
        {
            var result = 0.0;
            if (sizeChangedInfo.HeightChanged || sizeChangedInfo.WidthChanged && controll.ActualWidth.IsNotNaN())
            {
                result = controll.ActualWidth * multiply;
            }
            if (result < controll.ActualHeight - 10)
            {
                return result;
            }
            return current;
        }
    }
}
