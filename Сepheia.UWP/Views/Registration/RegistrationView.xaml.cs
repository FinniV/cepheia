﻿using Cepheia.Core.ViewModels.Registration;
using MvvmCross.ViewModels;

// The Blank Page item template is documented at https://go.microsoft.com/fwlink/?LinkId=234238

namespace Сepheia.UWP.Views.Registration
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    [MvxViewFor(typeof(RegistrationViewModel))]
    public sealed partial class RegistrationView : BaseView
    {
        public RegistrationView()
        {
            this.InitializeComponent();
        }
    }
}
