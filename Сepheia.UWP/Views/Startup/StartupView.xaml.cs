﻿using Cepheia.Core.ViewModels.StartUp;
using MvvmCross.ViewModels;

// The Blank Page item template is documented at https://go.microsoft.com/fwlink/?LinkId=234238

namespace Сepheia.UWP.Views
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    [MvxViewFor(typeof(StartupViewModel))]
    public sealed partial class StartupView : BaseView
    {
        public StartupView()
        {
            this.InitializeComponent();
            SizeChanged += (s, e) =>
            {
                loginTextBox.FontSize = GetSizeWithCheckInRealationTo(e, loginTextBox, 18d / 40d, loginTextBox.FontSize);
            };
        }

    }
}
