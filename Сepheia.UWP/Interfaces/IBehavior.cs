﻿using Windows.UI.Xaml;

namespace Сepheia.UWP.Interfaces
{
    public interface IBehavior
    {
        DependencyObject AssociatedObject { get; }
        void Attach(DependencyObject associatedObject);
        void Detach();
    }
}
