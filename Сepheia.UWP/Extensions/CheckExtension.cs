﻿using System;
namespace Сepheia.UWP
{
    public static class CheckExtension
    {
        public static bool IsNotNaN(this double self) => !Double.IsNaN(self) && !Double.IsInfinity(self);
    }
}