﻿using Cepheia.Core.Interfaces.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Сepheia.UWP.Implementations
{
    public class AlertUwpService : IAlertService
    {
        public void ShowErrorToast(string message) { }
        public void ShowInformationToast(string message) { }
        public void ShowSuccesToast(string message) { }
        public void ShowWarningToast(string message) { }
    }
}
