﻿using Cepheia.Core.Interfaces.Services.File;
using Cepheia.Monad.Monads;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Сepheia.UWP.Implementations
{
    public class OpenFileService : IFileOpenService
    {
        public Task<IO<byte[]>> OpenFile() => throw new NotImplementedException();

        public async Task<IO<byte[]>> OpenImageAsync()
        {
            var byteArrayFromImage = new IO<byte[]>();
            var picker = new Windows.Storage.Pickers.FileOpenPicker();
            picker.ViewMode = Windows.Storage.Pickers.PickerViewMode.Thumbnail;
            picker.SuggestedStartLocation = Windows.Storage.Pickers.PickerLocationId.PicturesLibrary;
            picker.FileTypeFilter.Add(".jpg");
            picker.FileTypeFilter.Add(".jpeg");
            picker.FileTypeFilter.Add(".png");

            var file = await picker.PickSingleFileAsync();

            if (file != null)
            {
                byteArrayFromImage.Value = File.ReadAllBytes(file.Path);
            }
            return byteArrayFromImage;
        }
    }
}
