﻿using Cepheia.Core.Interfaces.Services;
using Cepheia.Core.Interfaces.Services.File;
using Cepheia.Core.Interfaces.Services.Secure;
using MvvmCross;
using MvvmCross.Platforms.Uap.Core;
using MvvmCross.ViewModels;
using Сepheia.UWP.Implementations;

namespace Сepheia.UWP
{
    public class Setup<TApplication> : MvxWindowsSetup<TApplication> where TApplication : class, IMvxApplication, new()
    {
        protected override void InitializePlatformServices()
        {
            base.InitializePlatformServices();

            Mvx.IoCProvider.RegisterSingleton<IAlertService>(new AlertUwpService());
            Mvx.IoCProvider.RegisterSingleton<IFileOpenService>(new OpenFileService());
        }
    }
}
