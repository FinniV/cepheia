﻿using Cepheia.Models.User;
using Cepheia.Models.User.Roles;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Cepheia.DataAccess
{
    public class CepheiaDbContext: IdentityDbContext<ApplicationUser, ApplicationRole, string>
    {
        public CepheiaDbContext(DbContextOptions<CepheiaDbContext> options) : base(options)
        {

        }
        public DbSet<Developer> Developers { get; set; }
        public DbSet<ProjectManager> ProjectManagers { get; set; }
        public DbSet<GroupLeed> GroupLeeds { get; set; }
        public DbSet<QualityAssurance> QualityAssurances { get; set; }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            foreach (var property in builder.Model.GetEntityTypes()
                                                  .SelectMany(t => t.GetProperties())
                                                  .Where(p => p.ClrType == typeof(decimal)))
            {
                property.Relational().ColumnType = "decimal(18, 12)";
            }
            base.OnModelCreating(builder);
        }
    }
}
