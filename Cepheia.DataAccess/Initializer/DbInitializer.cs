﻿using Cepheia.Models.Enums.User;
using Cepheia.Models.User;
using Cepheia.Models.User.Additional;
using Cepheia.Models.User.Roles;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace Cepheia.DataAccess.Initializer
{
    public class DbInitializer
    {
        private readonly CepheiaDbContext _context;
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly RoleManager<ApplicationRole> _roleManager;
        private string _rootFolder;
        public DbInitializer(CepheiaDbContext context, UserManager<ApplicationUser> userManager, RoleManager<ApplicationRole> roleManager)
        {
            _context = context;
            _userManager = userManager;
            _roleManager = roleManager;
        }

        public void Initialize()
        {
            _context.Database.Migrate();

            if (!_roleManager.Roles.Any(x => x.Name == "Admin"))
            {
                SeedAdminAndRoles();
            }

            if (!_context.ProjectManagers.Any())
            {
                SeedProjectManagers();
            }

            if (!_context.Developers.Any())
            {
                SeedDevelopers();
            }

            if (!_context.QualityAssurances.Any())
            {
                QualityAssurances();
            }

            if (!_context.GroupLeeds.Any())
            {
                SeedGroupLeeds();
            }
        }

        private void SeedAdminAndRoles()
        {
            var admin = new ApplicationRole("Admin")
            {
                RoleType = UserRole.Admin
            };
            _roleManager.CreateAsync(admin).GetAwaiter().GetResult();
            var manager = new ApplicationRole("ProjectManager")
            {
                RoleType = UserRole.ProjectManager
            };
            _roleManager.CreateAsync(manager).GetAwaiter().GetResult();
            var leed = new ApplicationRole("GroupLeed")
            {
                RoleType = UserRole.GroupLeed
            };
            _roleManager.CreateAsync(leed).GetAwaiter().GetResult();
            var dev = new ApplicationRole("Developer")
            {
                RoleType = UserRole.Developer
            };
            _roleManager.CreateAsync(dev).GetAwaiter().GetResult();
            var qa = new ApplicationRole("QualityAssurance")
            {
                RoleType = UserRole.QualityAssurance
            };
            _roleManager.CreateAsync(qa).GetAwaiter().GetResult();

            var email = "admin@admin.com";
            var password = "admin1q2w!Q";
            var applicationUser = new ApplicationUser();
            applicationUser.UserName = email;
            applicationUser.Email = email;
            applicationUser.EmailConfirmed = true;
            applicationUser.LoginType = LoginType.Email;
            applicationUser.DateBirth = DateTime.Now;
            applicationUser.Gender = Gender.Other;
            applicationUser.FirstName = "Admin";
            applicationUser.LastName = "Admin";
            _userManager.CreateAsync(applicationUser, password).GetAwaiter().GetResult();
            var user = _userManager.FindByNameAsync(email).GetAwaiter().GetResult();
            _userManager.AddToRoleAsync(user, "Admin").GetAwaiter().GetResult();
            _context.SaveChangesAsync().GetAwaiter().GetResult();
        }

        private void SeedDevelopers()
        {
            var split = AppDomain.CurrentDomain.BaseDirectory.Split(new[] { "Cepheia.Api" }, StringSplitOptions.None);
            _rootFolder = split[0] + "Cepheia.Api\\wwwroot\\avatars\\";
            var developer = new Developer();
            var developerUser1 = new ApplicationUser();
            var developerUser1Email = "developer1@gmail.com";
            var developerUser1Password = "Test1234!!";
            developerUser1.UserName = developerUser1Email;
            developerUser1.Email = developerUser1Email;
            developerUser1.EmailConfirmed = true;
            developerUser1.LoginType = LoginType.Email;
            developerUser1.DateBirth = DateTime.Now;
            developerUser1.Gender = Gender.Male;
            developerUser1.FirstName = "Jack";
            developerUser1.LastName = "Johnson";
            developerUser1.Country = "Test";
            developerUser1.City = "Test";
            developerUser1.Postcode = "Test";
            developerUser1.PhoneNumber = "Test";
            developerUser1.Address1 = "Test";
            developerUser1.Address2 = "Test";

            var image = new UserImage
            {
                ImageGuid = Guid.NewGuid().ToString()
            };
            string path = _rootFolder + "admin.png";
            var fileInfo = new FileInfo(path);
            byte[] data = new byte[fileInfo.Length];
            using (var fs = fileInfo.OpenRead())
            {
                fs.Read(data, 0, data.Length);
            }
            image.Image = data;
            image.ImageName = "admin.jpg";

            developerUser1.UserImage = image;


            _userManager.CreateAsync(developerUser1, developerUser1Password).GetAwaiter().GetResult();
            developerUser1 = _userManager.FindByNameAsync(developerUser1Email).GetAwaiter().GetResult();
            _userManager.AddToRoleAsync(developerUser1, "Developer").GetAwaiter().GetResult();
            developer.Id = Guid.NewGuid().ToString();
            developer.User = developerUser1;
            

            _context.Developers.AddAsync(developer).GetAwaiter().GetResult();
            
            developer = new Developer();
            var developerUser2 = new ApplicationUser();
            var developerUser2Email = "developer2@gmail.com";
            var developerUser2Password = "Test1234!!";
            developerUser2.UserName = developerUser2Email;
            developerUser2.Email = developerUser2Email;
            developerUser2.EmailConfirmed = true;
            developerUser2.LoginType = LoginType.Email;
            developerUser2.DateBirth = DateTime.Now;
            developerUser2.Gender = Gender.Male;
            developerUser2.FirstName = "Mary";
            developerUser2.LastName = "Larson";
            developerUser2.Country = "Test";
            developerUser2.City = "Test";
            developerUser2.Postcode = "Test";
            developerUser2.PhoneNumber = "Test";
            developerUser2.Address1 = "Test";
            developerUser2.Address2 = "Test";

            image = new UserImage();
            image.ImageGuid = Guid.NewGuid().ToString();
            path = _rootFolder + "female.png";
            fileInfo = new FileInfo(path);
            data = new byte[fileInfo.Length];
            using (var fs = fileInfo.OpenRead())
            {
                fs.Read(data, 0, data.Length);
            }
            image.Image = data;
            image.ImageName = "2.jpg";

            developerUser2.UserImage = image;

            _userManager.CreateAsync(developerUser2, developerUser2Password).GetAwaiter().GetResult();
            developerUser2 = _userManager.FindByNameAsync(developerUser2Email).GetAwaiter().GetResult();
            _userManager.AddToRoleAsync(developerUser2, "Developer").GetAwaiter().GetResult();
            developer.Id = Guid.NewGuid().ToString();
            developer.User = developerUser2;

            _context.Developers.AddAsync(developer).GetAwaiter().GetResult();
            _context.SaveChangesAsync().GetAwaiter().GetResult();
        }

        private void SeedGroupLeeds()
        {
            var split = AppDomain.CurrentDomain.BaseDirectory.Split(new[] { "Cepheia.Api" }, StringSplitOptions.None);
            _rootFolder = split[0] + "Cepheia.Api\\wwwroot\\avatars\\";
            var fitnessGoals = new List<string>();
            fitnessGoals.Add("Fitness goal 1");
            fitnessGoals.Add("Fitness goal 2");

            var medicalConditions = new List<string>();
            medicalConditions.Add("Medical condition 1");

            var groupLeed = new GroupLeed();
            var groupLeedUser1 = new ApplicationUser();

            var groupLeedUser1Email = "gp@gmail.com";
            var groupLeedUser1Password = "Test1234!!";

            groupLeedUser1.UserName = groupLeedUser1Email;
            groupLeedUser1.Email = groupLeedUser1Email;
            groupLeedUser1.EmailConfirmed = true;
            groupLeedUser1.LoginType = LoginType.Email;
            groupLeedUser1.DateBirth = DateTime.Now;
            groupLeedUser1.Gender = Gender.Male;
            groupLeedUser1.FirstName = "Kate";
            groupLeedUser1.LastName = "Hudson";
            groupLeedUser1.Country = "Test";
            groupLeedUser1.City = "Test";
            groupLeedUser1.Postcode = "Test";
            groupLeedUser1.PhoneNumber = "Test";
            groupLeedUser1.Address1 = "Test";
            groupLeedUser1.Address2 = "Test";

            var image = new UserImage();
            image.ImageGuid = Guid.NewGuid().ToString();
            string path = _rootFolder + "male.png";
            FileInfo fileInfo = new FileInfo(path);
            byte[] data = new byte[fileInfo.Length];
            using (FileStream fs = fileInfo.OpenRead())
            {
                fs.Read(data, 0, data.Length);
            }
            image.Image = data;
            image.ImageName = "male.png";

            groupLeedUser1.UserImage = image;

            _userManager.CreateAsync(groupLeedUser1, groupLeedUser1Password).GetAwaiter().GetResult();
            groupLeedUser1 = _userManager.FindByNameAsync(groupLeedUser1Email).GetAwaiter().GetResult();
            _userManager.AddToRoleAsync(groupLeedUser1, "GroupLeed").GetAwaiter().GetResult();

            groupLeed.User = groupLeedUser1;
            _context.GroupLeeds.AddAsync(groupLeed).GetAwaiter().GetResult();
            _context.SaveChangesAsync().GetAwaiter().GetResult();
        }

        private void SeedProjectManagers()
        {
            var split = AppDomain.CurrentDomain.BaseDirectory.Split(new[] { "Cepheia.Api" }, StringSplitOptions.None);
            _rootFolder = split[0] + "Cepheia.Api\\wwwroot\\avatars\\";
            
            var projectManager = new ProjectManager();
            var projectManagerUser1 = new ApplicationUser();

            var pmUser1Email = "pm@gmail.com";
            var pmUser1Password = "Test1234!!";

            projectManagerUser1.UserName = pmUser1Email;
            projectManagerUser1.Email = pmUser1Email;
            projectManagerUser1.EmailConfirmed = true;
            projectManagerUser1.LoginType = LoginType.Email;
            projectManagerUser1.DateBirth = DateTime.Now;
            projectManagerUser1.Gender = Gender.Female;
            projectManagerUser1.FirstName = "Kate";
            projectManagerUser1.LastName = "Hudson";
            projectManagerUser1.Country = "Test";
            projectManagerUser1.City = "Test";
            projectManagerUser1.Postcode = "Test";
            projectManagerUser1.PhoneNumber = "Test";
            projectManagerUser1.Address1 = "Test";
            projectManagerUser1.Address2 = "Test";

            var image = new UserImage();
            image.ImageGuid = Guid.NewGuid().ToString();
            string path = _rootFolder + "male.png";
            FileInfo fileInfo = new FileInfo(path);
            byte[] data = new byte[fileInfo.Length];
            using (FileStream fs = fileInfo.OpenRead())
            {
                fs.Read(data, 0, data.Length);
            }
            image.Image = data;
            image.ImageName = "male.png";

            projectManagerUser1.UserImage = image;

            _userManager.CreateAsync(projectManagerUser1, pmUser1Password).GetAwaiter().GetResult();
            projectManagerUser1 = _userManager.FindByNameAsync(pmUser1Email).GetAwaiter().GetResult();
            _userManager.AddToRoleAsync(projectManagerUser1, "ProjectManager").GetAwaiter().GetResult();

            projectManager.User = projectManagerUser1;
            _context.ProjectManagers.AddAsync(projectManager).GetAwaiter().GetResult();
            _context.SaveChangesAsync().GetAwaiter().GetResult();
        }

        private void QualityAssurances()
        {
            var split = AppDomain.CurrentDomain.BaseDirectory.Split(new[] { "Cepheia.Api" }, StringSplitOptions.None);
            _rootFolder = split[0] + "Cepheia.Api\\wwwroot\\avatars\\";
            
            var qualityAssurance = new QualityAssurance();
            var qualityAssuranceUser1 = new ApplicationUser();

            var qualityAssuranceUser1Email = "qa@gmail.com";
            var qualityAssuranceUser1Password = "Test1234!!";

            qualityAssuranceUser1.UserName = qualityAssuranceUser1Email;
            qualityAssuranceUser1.Email = qualityAssuranceUser1Email;
            qualityAssuranceUser1.EmailConfirmed = true;
            qualityAssuranceUser1.LoginType = LoginType.Email;
            qualityAssuranceUser1.DateBirth = DateTime.Now;
            qualityAssuranceUser1.Gender = Gender.Female;
            qualityAssuranceUser1.FirstName = "Kate";
            qualityAssuranceUser1.LastName = "Hudson";
            qualityAssuranceUser1.Country = "Test";
            qualityAssuranceUser1.City = "Test";
            qualityAssuranceUser1.Postcode = "Test";
            qualityAssuranceUser1.PhoneNumber = "Test";
            qualityAssuranceUser1.Address1 = "Test";
            qualityAssuranceUser1.Address2 = "Test";

            var image = new UserImage();
            image.ImageGuid = Guid.NewGuid().ToString();
            string path = _rootFolder + "male.png";
            var fileInfo = new FileInfo(path);
            byte[] data = new byte[fileInfo.Length];
            using (var fs = fileInfo.OpenRead())
            {
                fs.Read(data, 0, data.Length);
            }
            image.Image = data;
            image.ImageName = "male.png";

            qualityAssuranceUser1.UserImage = image;

            _userManager.CreateAsync(qualityAssuranceUser1, qualityAssuranceUser1Password).GetAwaiter().GetResult();
            qualityAssuranceUser1 = _userManager.FindByNameAsync(qualityAssuranceUser1Email).GetAwaiter().GetResult();
            _userManager.AddToRoleAsync(qualityAssuranceUser1, "QualityAssurance").GetAwaiter().GetResult();

            qualityAssurance.User = qualityAssuranceUser1;

            _context.QualityAssurances.AddAsync(qualityAssurance).GetAwaiter().GetResult();
            _context.SaveChangesAsync().GetAwaiter().GetResult();
        }
    }
}
