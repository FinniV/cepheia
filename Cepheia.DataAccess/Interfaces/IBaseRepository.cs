﻿using Cepheia.Models.BaseModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cepheia.DataAccess.Interfaces
{
    public interface IBaseRepository<TEntity> where TEntity : BaseModel
    {
        Task SaveChanges();
        Task<int> Count();
        void DeleteTrackedEntities();
        Task DeleteByIds(List<string> ids);
        Task Delete(string id);
        Task Update(IEnumerable<TEntity> entity);
        Task Update(string id, TEntity entity);
        Task Create(IEnumerable<TEntity> entities);
        Task Create(TEntity entity);
        Task<TEntity> GetById(string id);
        IQueryable<TEntity> GetAll();
    }
}
