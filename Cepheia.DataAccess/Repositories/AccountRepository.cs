﻿using Cepheia.Models.Enums.User;
using Cepheia.Models.User;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cepheia.DataAccess.Repositories
{
    public class AccountRepository
    {
        private readonly UserManager<ApplicationUser> _userManager;

        public AccountRepository(UserManager<ApplicationUser> userManager)
        {
            _userManager = userManager;
        }

        public async Task<ApplicationUser> GetUserById(string id)
        {
            var result = await _userManager.Users.Include(x => x.UserImage).FirstOrDefaultAsync(x => x.Id == id);
            return result;
        }

        public async Task<ApplicationUser> GetUserByEmailAndLoginType(string email, LoginType loginType)
        {
            var result = await _userManager.Users.Include(x => x.UserImage).FirstOrDefaultAsync(x => x.Email == email && x.LoginType == loginType);
            return result;
        }

        public async Task<string> GenerateTemporaryPassword(ApplicationUser user)
        {
            var password = Guid.NewGuid().ToString().Substring(0, 8);
            var token = await _userManager.GeneratePasswordResetTokenAsync(user);
            await _userManager.ResetPasswordAsync(user, token, password);
            return password;
        }

        public async Task<bool> GeneratePasswordResetToken(ApplicationUser user)
        {
            var resetToken = await _userManager.GeneratePasswordResetTokenAsync(user);
            user.ResetPasswordToken = resetToken;
            var result = await _userManager.UpdateAsync(user);
            return result.Succeeded;
        }

        public async Task<IdentityResult> ResetPassword(ApplicationUser user, string password)
        {
            var result = await _userManager.ResetPasswordAsync(user, user.ResetPasswordToken, password);
            if (!result.Succeeded)
            {
                return result;
            }

            user.ResetPasswordCode = null;
            user.ResetPasswordToken = null;
            result = await _userManager.UpdateAsync(user);
            return result;
        }

        public async Task<List<string>> GetRoles(ApplicationUser user)
        {
            var roles = await _userManager.GetRolesAsync(user);
            return roles.ToList();
        }

        public async Task<IdentityResult> AddToRole(ApplicationUser user, string role)
        {
            var result = await _userManager.AddToRoleAsync(user, role);
            return result;
        }

        public async Task<IdentityResult> CreateUser(ApplicationUser user)
        {
            IdentityResult result = await _userManager.CreateAsync(user);
            return result;
        }

        public async Task<IdentityResult> CreateUserWithPassword(ApplicationUser user, string password)
        {
            IdentityResult result = await _userManager.CreateAsync(user, password);
            return result;
        }

        public async Task<bool> ConfirmEmail(ApplicationUser user, string code)
        {
            if (user.ConfirmEmailToken != code)
            {
                return false;
            }

            user.EmailConfirmed = true;
            user.ConfirmEmailToken = null;
            var result = await _userManager.UpdateAsync(user);
            return result.Succeeded;
        }

        public async Task<string> GenerateEmailConfirmationToken(ApplicationUser user)
        {
            var token = await _userManager.GenerateEmailConfirmationTokenAsync(user);
            return token;
        }
    }
}
