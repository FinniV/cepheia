﻿using Cepheia.DataAccess.Interfaces;
using Cepheia.Models.BaseModels;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cepheia.DataAccess.Repositories
{
    public class BaseRepository<TEntity> : IBaseRepository<TEntity> where TEntity : BaseModel
    {
        protected readonly CepheiaDbContext _dbContext;

        public BaseRepository(CepheiaDbContext context)
        {
            _dbContext = context;
        }

        public IQueryable<TEntity> GetAll()
        {
            return _dbContext.Set<TEntity>().AsNoTracking();
        }

        public async Task<TEntity> GetById(string id)
        {
            return await _dbContext.Set<TEntity>()
                .AsNoTracking()
                .FirstOrDefaultAsync(e => e.Id == id);
        }

        public async Task Create(TEntity entity)
        {
            DeleteTrackedEntities();
            await _dbContext.Set<TEntity>().AddAsync(entity);
            await _dbContext.SaveChangesAsync();
        }

        public async Task Create(IEnumerable<TEntity> entities)
        {
            DeleteTrackedEntities();
            await _dbContext.Set<TEntity>().AddRangeAsync(entities);
            await _dbContext.SaveChangesAsync();
        }

        public async Task Update(string id, TEntity entity)
        {
            DeleteTrackedEntities();
            _dbContext.Set<TEntity>().Update(entity);
            await _dbContext.SaveChangesAsync();
        }

        public async Task Update(IEnumerable<TEntity> entity)
        {
            DeleteTrackedEntities();
            _dbContext.Set<TEntity>().UpdateRange(entity);
            await _dbContext.SaveChangesAsync();
        }

        public async Task Delete(string id)
        {
            var entity = await GetById(id);
            _dbContext.Set<TEntity>().Remove(entity);
            await _dbContext.SaveChangesAsync();
        }

        public async Task DeleteByIds(List<string> ids)
        {
            if (ids.Count == 0)
            {
                return;
            }

            DeleteTrackedEntities();
            foreach (var id in ids)
            {
                await Delete(id);
            }
        }
        
        public async Task<int> Count()
        {
            return await _dbContext.Set<TEntity>().AsNoTracking().CountAsync();
        }

        public virtual async Task SaveChanges()
        {
            try
            {
                await _dbContext.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException ex)
            {
                throw ex;
            }
        }

        public void DeleteTrackedEntities()
        {
            var changedEntriesCopy = _dbContext.ChangeTracker.Entries().ToList();
            foreach (var entity in changedEntriesCopy)
            {
                _dbContext.Entry(entity.Entity).State = EntityState.Detached;
            }
        }
    }
}
