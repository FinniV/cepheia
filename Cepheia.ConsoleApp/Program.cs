﻿using Cepheia.Models.User.Roles;
using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;

namespace Cepheia.ConsoleApp
{
    class Program
    {
        static void Main(string[] args)
        {
            var context = new SQLiteDataAcces.Context.SQLiteDbContext();
            
            var applicationUser = context.ApplicationUsers.ToArray();
            var developers = context.Developers.Include(x=>x.User).ToList();
          
            var developersWithLowSallary = developers
                .Select(x => x)
                .Where(d => d.Salary < 600)
                .Select(d => 
                {
                    d.Salary += 100;
                    return d;
                }).ToList();
            context.Developers.UpdateRange(developersWithLowSallary);
            context.SaveChanges();
            foreach (var item in developers)
            {
                Console.Write(string.Format("ID : {0}  Name : {1}  Salary : {2} {3}", item.Id, item.User.FirstName, item.Salary, Environment.NewLine));
            }
            Console.Write("Developer with salary more than 200\n");
            Console.WriteLine("-------------------\n");
           
            context.Developers.RemoveRange(context.Developers.Include(x=>x.User));
            context.ApplicationUsers.RemoveRange(context.ApplicationUsers);
            context.SaveChanges();
            applicationUser = context.ApplicationUsers.ToArray();
            foreach (var item in developers)
            {
                Console.Write(string.Format("ID : {0}  Name : {1}  Salary : {2} {3}", item.Id, item.User.FirstName, item.Salary, Environment.NewLine));
            }
            Console.ReadKey();
        }
    }
}
