﻿using Cepheia.Models.User.Roles;
using Cepheia.SQLiteDataAcces.Context;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Cepheia.SQLiteDataAcces
{
    public class DbInitializer
    {
        private SQLiteDbContext Context;

        public DbInitializer(SQLiteDbContext context)
        {
            Context = context;
            Initialize();
        }

        private void Initialize()
        {
            if (!Context.Developers.Any())
            {
                SeedDevelopers(Context);
            }
        }

        private void SeedDevelopers(SQLiteDbContext context)
        {
            var firstDeveloper = new Developer
            {
                Salary = 200,
                User = new Models.User.ApplicationUser
                {
                    FirstName = "John",
                    LastName = "Doe",
                    Address1 = "First address",
                    Address2 = "Second address",
                    City = "Kharkiv",
                    Country = "Ukrain",
                    DateBirth = DateTime.Now.AddYears(-20),
                    Gender = Models.Enums.User.Gender.Male,
                    Postcode = "61000",
                    LoginType = Models.Enums.User.LoginType.Email,
                }
            };
        }
    }
}
