﻿using Cepheia.Models.User;
using Cepheia.Models.User.Roles;
using Microsoft.EntityFrameworkCore;
using System;
using System.IO;
using System.Linq;

namespace Cepheia.SQLiteDataAcces.Context
{
    public class SQLiteDbContext : DbContext
    {
        public SQLiteDbContext() => Database.EnsureCreated();
        public static string ApplicationFolder(string filename = "") => Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments), filename);

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            base.OnConfiguring(optionsBuilder);

            var _databasePath = DatabaseFolder();
            optionsBuilder.UseSqlite($"Filename={_databasePath}");
        }

        public static string DatabaseFolder(string filename = "db.sqlite3") => ApplicationFolder(filename);

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            modelBuilder.Entity<Developer>(entity =>
            {
                entity.HasKey(p => p.Id);
            });
        }
        public DbSet<Developer> Developers { get; set; }
        public DbSet<ApplicationUser> ApplicationUsers { get; set; }
    }

}

