﻿using Cepheia.WPF.View.Base;
using System.Windows;

namespace Cepheia.WPF.View
{
    /// <summary>
    /// Interaction logic for StartupView.xaml
    /// </summary>
    public partial class StartupView : BaseView
    {
        public StartupView() => this.InitializeComponent();
        protected override void OnRenderSizeChanged(SizeChangedInfo sizeInfo)
        {
            base.OnRenderSizeChanged(sizeInfo);
            if (loginTextBox.ActualHeight.IsNotNaN())
            {
                passwordBox.FontSize = loginTextBox.FontSize = GetSizeWithCheckInRealationTo(sizeInfo,loginTextBox, 0.342857143,passwordBox.FontSize);
            }
        }

    }
}
