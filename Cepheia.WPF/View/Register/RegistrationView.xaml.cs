﻿using System.Windows;
using Cepheia.WPF.View.Base;

namespace Cepheia.WPF.View.Register
{
    /// <summary>
    /// Interaction logic for RegistrationView.xaml
    /// </summary>
    public partial class RegistrationView : BaseView
    {
        public RegistrationView()
        {
            InitializeComponent();

        }

        protected override void OnRenderSizeChanged(SizeChangedInfo sizeInfo)
        {
            base.OnRenderSizeChanged(sizeInfo);
            deletePhotoButton.FontSize = GetSizeInRealationToWidth(sizeInfo, deletePhotoButton, 22d / 160d,deletePhotoButton.FontSize);

        }
    }
}
