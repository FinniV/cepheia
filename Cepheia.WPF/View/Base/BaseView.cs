﻿using MvvmCross.Wpf.Views;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;

namespace Cepheia.WPF.View.Base
{
    public class BaseView : MvxWpfView
    {

        protected double GetSizeWithCheckInRealationTo<TControll>(SizeChangedInfo sizeChangedInfo, TControll controll, double multiply,double current) where TControll : Control
        {
            if (sizeChangedInfo.HeightChanged&& sizeChangedInfo.WidthChanged)
            {
                return controll.ActualHeight * multiply;
            }
            return current;
        }

        protected double GetSizeInRealationToHeight<TControll>(TControll controll,double multiply) where TControll : Control
        {
            return controll.ActualHeight * multiply;
        }

        protected double GetSizeInRealationToWidth<TControll>(SizeChangedInfo sizeChangedInfo, TControll controll, double multiply, double current) where TControll : Control
        {
            var result = 0.0;
            if (sizeChangedInfo.HeightChanged||sizeChangedInfo.WidthChanged && controll.ActualWidth.IsNotNaN())
            {
                result = controll.ActualWidth * multiply;
            }
            if (result<controll.ActualHeight-10)
            {
                return result;
            }
            return current;
        }
    }
}
