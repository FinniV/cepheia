﻿using Cepheia.Core.Interfaces.Services;
using Cepheia.Core.Interfaces.Services.File;
using Cepheia.Core.Interfaces.Services.Secure;
using Cepheia.WPF.Converters.ImageConverters;
using Cepheia.WPF.Implementations;
using Cepheia.WPF.Implementations.Services.FileServices;
using MvvmCross.Binding.Wpf;
using MvvmCross.Converters;
using MvvmCross.Core.ViewModels;
using MvvmCross.Platform;
using MvvmCross.Platform.IoC;
using System.Windows.Threading;
using MvxWpfSetup = MvvmCross.Wpf.Platform.MvxWpfSetup;

namespace Cepheia.WPF
{
    public class Setup : MvxWpfSetup
    {
        public Setup(Dispatcher dispatcher, MvvmCross.Wpf.Views.Presenters.IMvxWpfViewPresenter presenter)
            : base(dispatcher, presenter)
        {
        }

        protected override IMvxApplication CreateApp()
        {
            CreatableTypes()
                .EndingWith("Service")
                .AsInterfaces()
                .RegisterAsLazySingleton();
            return new Core.App();
        }
        
        protected override void InitializePlatformServices()
        {
            Mvx.RegisterSingleton<IAlertService>(new AlertWpfService());
            Mvx.RegisterSingleton<ITokenStorageService>(new TokenStorageService());
            Mvx.RegisterSingleton<IFileOpenService>(new FileOpenService());

        }
    }
}
