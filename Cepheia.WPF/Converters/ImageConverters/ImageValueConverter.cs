﻿using Cepheia.WPF.Constants;
using Cepheia.WPF.Properties;
using System;
using System.Diagnostics;
using System.Drawing.Imaging;
using System.Globalization;
using System.IO;
using System.Windows.Controls;
using System.Windows.Media.Imaging;

namespace Cepheia.WPF.Converters.ImageConverters
{
    public class ImageValueConverter : System.Windows.Data.IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            var result = new Image();
            var bitmap = new BitmapImage();
            try
            {
                if (value is byte[] bytes)
                {
                    using (var mem = new MemoryStream(bytes))
                    {
                        bitmap.BeginInit();
                        bitmap.CreateOptions = BitmapCreateOptions.PreservePixelFormat;
                        bitmap.CacheOption = BitmapCacheOption.OnLoad;
                        bitmap.UriSource = null;
                        bitmap.StreamSource = mem;
                        bitmap.EndInit();
                    }
                    result.Source = bitmap;
                    return result;
                }

                string bitmapPath = @"Cepheia.WPF;component/Resources/default_user.png";
                bitmap = new BitmapImage(new Uri(bitmapPath, UriKind.Relative));
                result.Source = bitmap;
                return result;

            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex);
                result.Source = new BitmapImage(new Uri(ResourcePathConstants.Resources + "\\default-user.png"));
                return result;
            }

        }
        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture) => throw new NotImplementedException();

    }
}
 