﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cepheia.WPF.Constants
{
    public static class ResourcePathConstants
    {
        public static readonly string Resources = AppDomain.CurrentDomain.BaseDirectory.Split(new[] { "Cepheia.WPF" }, StringSplitOptions.None)[0]+ "Cepheia.WPF" + "\\Resources";
    }
}
