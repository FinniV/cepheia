﻿using Cepheia.Core.Interfaces.Services.File;
using Cepheia.Monad.Monads;
using Microsoft.Win32;
using System.IO;
using System.Threading.Tasks;

namespace Cepheia.WPF.Implementations.Services.FileServices
{
    public class FileOpenService : IFileOpenService
    {
        public async Task<IO<byte[]>> OpenFile()
        {
            var byteArrayFromFile = new IO<byte[]>();
            var openFileDialog = new OpenFileDialog();
            if (openFileDialog.ShowDialog() == true)
            {
                byteArrayFromFile.Value = File.ReadAllBytes(openFileDialog.FileName);
            }
            return byteArrayFromFile;
        }
        public async Task<IO<byte[]>> OpenImage()
        {
            var byteArrayFromImage = new IO<byte[]>();
            var openFileDialog = new OpenFileDialog
            {
                Filter = "Image files (*.png;*.jpeg)|*.png;*.jpeg|All files (*.*)|*.*"
            };
            if (openFileDialog.ShowDialog() == true)
            {
                byteArrayFromImage.Value = File.ReadAllBytes(openFileDialog.FileName);
            }
            return byteArrayFromImage;
        }
    }
}
