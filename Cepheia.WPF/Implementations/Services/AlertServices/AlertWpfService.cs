﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using Cepheia.Core.Interfaces.Services;
using ToastNotifications;
using ToastNotifications.Lifetime;
using ToastNotifications.Messages;
using ToastNotifications.Position;

namespace Cepheia.WPF.Implementations
{
    public class AlertWpfService : IAlertService
    {
        private Notifier _notifier = new Notifier(cfg =>
        {
            cfg.PositionProvider = new WindowPositionProvider(
                Application.Current.MainWindow,
                Corner.BottomCenter,
                25,
                100);

            cfg.LifetimeSupervisor = new TimeAndCountBasedLifetimeSupervisor(
                TimeSpan.FromSeconds(6),
                MaximumNotificationCount.FromCount(6));

            cfg.Dispatcher = Application.Current.Dispatcher;

            cfg.DisplayOptions.TopMost = false;
            cfg.DisplayOptions.Width = 250;
        });
        public void ShowSuccesToast(string message)
        {
            _notifier.ShowSuccess(message);
        }

        public void ShowInformationToast(string message)
        {
            _notifier.ShowInformation(message);
        }

        public void ShowWarningToast(string message)
        {
            _notifier.ShowWarning(message);
        }

        public void ShowErrorToast(string message)
        {
            _notifier.ShowError(message);
        }
    }
}
