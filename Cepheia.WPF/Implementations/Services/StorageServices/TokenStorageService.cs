﻿using Cepheia.Core.Interfaces.Services.Secure;
using Microsoft.Win32;
using System;
using System.Diagnostics;
using System.Reflection;

namespace Cepheia.WPF.Implementations
{
    public class TokenStorageService : ITokenStorageService
    {
        private static readonly string appName = AppDomain.CurrentDomain.FriendlyName.Split(".".ToCharArray())[0];
        
        private static readonly string fileVersionInfo = FileVersionInfo.GetVersionInfo(Assembly.GetExecutingAssembly().Location).ProductVersion;

        public string GetToken()
        {
            using (var key = Registry.CurrentUser.OpenSubKey($"Software\\{appName}\\{fileVersionInfo}"))
            {
                if (key != null)
                {
                    var token = key.GetValue("Token");
                    if (token != null)
                    {
                        return token as string;
                    }
                }
                return "Can`t get token";
            }
        }
        public bool WriteToken(string token)
        {
            try
            {
                var key = Registry.CurrentUser.OpenSubKey("Software", true);
                key.CreateSubKey(appName);
                key = key.OpenSubKey(appName, true);
                key.CreateSubKey(fileVersionInfo);
                key = key.OpenSubKey(fileVersionInfo, true);

                key.SetValue("Token", token);
                return true;
            }
            catch (Exception e)
            {
                Debug.WriteLine(e);
                return false;
            }
        }
    }
}
